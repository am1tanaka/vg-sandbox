# vg-sandbox
VoxelGameプロジェクト用ひな形のサンドボックスです。

# 方針
- Unity2018.1.3向けプロジェクトです
- Systemシーンは常にゲームに存在するシーンです
- 以下の機能は、Systemシーンに配置してあるGameSystemクラスに実装されているstaticメソッドを呼び出して実行します
    - シーンの切り替え(自動的にフェード処理が入ります)
    - フラグの変更
    - メニューなどのUIの表示、非表示、表示状態の確認
- オーディオ関連は、AudioPlayerクラスを呼び出して実行します
    - AudioPlay.PlayBGM(BGMのID[, フェードイン秒数を指定]);
    - AudioPlay.PlaySE(SEのID);
    - AudioPlay.StopBGM([フェードアウト秒数]);
    - AudioPlay.StopSE(全てのSEを停止);
- PluginsフォルダーとPrivateフォルダーはGit管理外にします。公式アセットや公開したくないアセットはこの中へ入れて、Googleドライブの該当フォルダーにデータを置いてください


# WIP
- TextMesh ProやPostProcessing Stackを少ししたら組み込みます

# 履歴
- 2018/6/19 Unityのバージョン決定/Git管理外フォルダーの追加/TextMesh ProとPPSをWIPに
