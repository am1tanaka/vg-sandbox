﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VG {
    public class AnimatorEvent : MonoBehaviour {

        public void MessageWindowOpenDone() {
            MessageManager.Instance.MessageWindowOpenDone();
        }
    }
}
