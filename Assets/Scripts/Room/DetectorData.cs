﻿using UnityEngine;

[CreateAssetMenu(menuName ="VG/Create Detector Data")]
public class DetectorData : ScriptableObject {

    [Header("発動条件データセット")]
    [Tooltip("クリックした時にイベントが起きるのに必要な条件を設定します。複数設定すると、そのすべてが成立した時に、このイベントが発動します。")]
    public EventDataSet[] triggerRules;
    [Header("発動した時に変更するデータセット")]
    [Tooltip("イベントの結果、変更するデータを設定します。")]
    public EventDataSet[] changeDataSet;
    [Header("キャラクターの行動トリガー名")]
    [Tooltip("イベントを実行する時に再生するアニメーションのトリガー名を設定します。省略するとActionトリガーを呼び出します。")]
    public string actionName;
}
