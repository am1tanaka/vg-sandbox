﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// ゲームシーンの管理クラスです。
/// </summary>
public class GameManager : MonoBehaviour {

    private void Awake() {
        /*
        // 必要な他のシーンを読み込む
        SceneManager.LoadScene("GameUI", LoadSceneMode.Additive);
        */
    }

    void Start() {
        SoundPlayer.PlayBGM(SoundPlayer.BGM.GAME);
    }

    // Update is called once per frame
    void Update () {
        if (!GameSystem.IsControllable) {
            return;
        }
	}

    public void ToTitle() {
        SoundPlayer.PlaySE(SoundPlayer.SE.ui_menu_selectitem);
        GameSystem.ChangeScene("Title");
    }
}
