﻿// #define EVENTSEQUENCER_DEBUG

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AM1.Nav;

namespace VG {
    /// <summary>
    /// プレイヤーキャラクターにアタッチするイベントを進行するためのスクリプト。
    /// ClickDetectorから呼び出す。
    /// </summary>
    public class EventSequencer : MonoBehaviour {

        public static EventSequencer Instance { get; private set; }
        public static bool isRunning { get; private set; }

        Animator anim;
        NavController sendObject;
        bool isStartAnimation;

        private void Awake() {
            Instance = this;
            anim = GetComponentInChildren<Animator>();
            isRunning = false;
            sendObject = GetComponent<NavController>();
        }

        public IEnumerator EventSequence(ClickDetector clickDetector, bool isWalk=true) {
            DetectorData data = clickDetector.detectorData[clickDetector.detectIndex];
            isRunning = true;
            isStartAnimation = false;

            // キャラクターを歩かせる
            if (isWalk) {
                if (clickDetector.isFixedTargetPosition) {
                    sendObject.SetDestination(clickDetector.startPosition + clickDetector.targetPosition);
                }
                else {
                    sendObject.SetDestination(clickDetector.transform.position + clickDetector.targetPosition);
                }

                /// 移動完了を待つ
                while (!sendObject.Reached) {
                    yield return null;
                }

                /// 対象方向を向く
                if (clickDetector.isFixedLookPosition) {
                    sendObject.SetLookRotation(clickDetector.startPosition + clickDetector.lookPosition);
                }
                else {
                    sendObject.SetLookRotation(clickDetector.transform.position + clickDetector.lookPosition);
                }

                /// 回転完了を待つ
                while (!sendObject.Rotated) {
                    yield return null;
                }

                // アニメを呼び出して終了待ち
                string ws = "Action";
                if (data.actionName.Length > 0) {
                    ws = data.actionName;
                }
                DebugLog("anime=" + ws);
                if (ws != "NoAction") {
                    // アニメ開始。次の処理を進められる状態になったら処理が戻る
                    isStartAnimation = true;
                    yield return sendObject.AnimeAndWaitNext(ws);
                }
            }

            // データ反映
            DebugLog("start data change");
            if (data.changeDataSet.Length > 0) {
                DataStore.Instance.Change(data.changeDataSet, onChangeDataDone);
                yield break;
            }

            onChangeDataDone();
        }

        /// <summary>
        /// DataStore.Instance.Change()が完了した時に呼び出すコールバック関数
        /// </summary>
        void onChangeDataDone() {
            StartCoroutine(waitSequenceDone());
        }

        IEnumerator waitSequenceDone() {
            // 完了を待つ
            DebugLog("wait done : " + sendObject + " isAnimeDone = " + sendObject.isAnimeDone);
            while (isStartAnimation && !sendObject.isAnimeDone) {
                DebugLog("wait done : isDataChanging=" + sendObject + " isAnimeDone = " + sendObject.isAnimeDone + " " + Time.time);
                yield return null;
            }

            DebugLog("sequence done.");
            isRunning = false;
        }

        [System.Diagnostics.Conditional("EVENTSEQUENCER_DEBUG")]
        static void DebugLog(object msg) {
            Debug.Log(msg);
        }

    }
}
