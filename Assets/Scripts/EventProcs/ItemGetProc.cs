﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace VG {
    public class ItemGetProc : MonoBehaviour {

        EventInvoker eventInvoker;

        private void Awake() {
            eventInvoker = GetComponent<EventInvoker>();
        }

        /// <summary>
        /// EventInvokerのOn Initに設定するメソッド
        /// </summary>
        /// <param name="index"></param>
        public void OnInit() {
            Debug.Log("set event "+name);
        }

        /// <summary>
        /// EventInvokerのOn Changeに設定するメソッド
        /// </summary>
        public void OnChange() {
            ItemMaster.ItemType getItem = ItemMaster.ItemType.None;
            EventDataSet eset = eventInvoker.GetEventDataSetWithValue("get");
            // getがなければ何もしない
            if (eset == null) {
                eventInvoker.ChangeProcLoop();
                return;
            }

            getItem = ItemListManager.Instance.GetItemType(eset.name);
            //Debug.Log("getItem="+getItem+"/esetname="+ eset.name);

            if (getItem == ItemMaster.ItemType.None) {
                eventInvoker.ChangeProcLoop();
                return;
            }

            // アイテムを手に入れる
            ItemListManager.Instance.GetItem(getItem);
            string mes = string.Format(
                MessageManager.Instance.GetMessage("get-item"),
                ItemListManager.itemMaster[getItem].itemName);
            MessageManager.Instance.ShowMessage(mes);
            gameObject.SetActive(false);
            eventInvoker.ChangeProcLoop();
        }
    }
}
