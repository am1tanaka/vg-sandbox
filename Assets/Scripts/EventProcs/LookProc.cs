﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace VG {
    /**
     * EventInvokerと一緒に、対象のオブジェクトにアタッチします。
     */
    public class LookProc : MonoBehaviour {

        [Tooltip("切り替えるカメラ"), SerializeField]
        Camera targetCamera;
        [Tooltip("カメラ切り替え開始からクリックを有効にするまでの秒数"), SerializeField]
        float clickStartTime = 1f;

        float startTime;
        bool isDoing = false;
        EventInvoker eventInvoker;

        private void Awake() {
            startTime = clickStartTime;
            isDoing = false;
            eventInvoker = GetComponent<EventInvoker>();            
        }

        private void Update() {
            startTime += Time.deltaTime;
            if (!isDoing || (startTime < clickStartTime)) return;

            // クリック待ち
            if (Input.GetMouseButtonDown(0)) {
                isDoing = false;
                CameraChanger.Instance.AddCameraIndex(0);
                eventInvoker.OnAnimeDone();
                SoundPlayer.PlaySE(SoundPlayer.SE.se_camera_zoomout);
            }
        }

        /// <summary>
        /// カメラの切り替え開始
        /// </summary>
        public void OnChange() {
            // カメラ移動開始
            EventInvoker.AnimeStart();
            startTime = 0;
            isDoing = true;
            CameraChanger.Instance.SetTargetCamera(targetCamera);
        }
    }
}
