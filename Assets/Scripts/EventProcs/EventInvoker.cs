﻿//#define EVENTINVOKER_DEBUG

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace VG {
    [System.Serializable]
    public struct EventProcedureData {
        [Tooltip("発動条件のEventData")]
        public EventData checkData;
        [Tooltip("この条件が成立している時の初期化用コード")]
        public UnityEvent onInit;
        [Tooltip("この条件になった時に呼び出すコード。EventInvokerクラスのSet系メソッドのみ登録すること。")]
        public UnityEvent onChange;
        [Tooltip("変更時に鳴らす効果音")]
        public SoundPlayer.SE onChangeSE;
    }

    public class EventInvoker : MonoBehaviour {

        #region Serialize Fields

        [Tooltip("イベントの状態ごとの設定処理、変更処理を登録します。"), SerializeField]
        EventProcedureData[] eventProcData;

#endregion Serialize Fields

        #region Variables

        static int animeCount;
        protected int lastIndex = -1;
        Animator anim;
        EventDataSet[] changedStoreData;
        EventDataSet[] changingData;
        UnityAction changeDoneHandler;

        string animatorParamName = "";
        bool animatorFlag = false;

        /// <summary>
        /// 動作実行後に呼び出す処理をこれに記録します。
        /// </summary>
        protected UnityAction doneProc;

        #endregion Variables

        #region Unity System

        private void Awake() {
            anim = GetComponent<Animator>();
            animeCount = 0;
        }

        #endregion Unity System

        #region Static Methods

        public static void ClearAnimeCount() {
            animeCount = 0;
        }

        /// <summary>
        /// アニメカウントが0になるまで待ちます
        /// </summary>
        /// <returns>コルーチンインスタンス</returns>
        public static IEnumerator WaitDone() {
            while (animeCount > 0) {
                yield return null;
            }
            yield return null;
        }

        /// <summary>
        /// アニメ開始時に呼び出します。
        /// </summary>
        public static void AnimeStart() {
            animeCount++;
        }

        /// <summary>
        /// アニメが終了した時に呼び出します。
        /// </summary>
        public static void AnimeDone() {
            animeCount--;
            if (animeCount < 0) {
#if UNITY_EDITOR
                DebugLog("animeCountがマイナスになりました。回数が一致していません。");
#endif
                animeCount = 0;
            }
        }

        #endregion Static Methods

        #region EventProcedure

        /// <summary>
        /// データを設定した時の処理を実装します。
        /// </summary>
        /// <param name="eventdata">更新したDataStoreのstore</param>
        public void SetProc(EventDataSet[] eventdata) {
            int idx = IndexOfCheckData(eventdata, 0);
            DebugLog("SetProc " + idx);
            while (idx != -1) {
                eventProcData[idx].onInit?.Invoke();
                idx = IndexOfCheckData(eventdata, idx + 1);
                DebugLog("SetProc loop " + idx);
            }
        }

        /// <summary>
        /// データを変更した時の処理を実装します。
        /// </summary>
        /// <param name="eventdata">更新後のDataStoreの全状態データ</param>
        /// <param name="changes">今回変更したデータのセット。このデータに関係ないイベントは発動させない</param>"
        /// <param name="done">DataStoreのchangeLoopDone。イベント処理が完了したら、残りのイベントを処理するためのループに戻す先</param>
        public void ChangeProc(EventDataSet[] eventdata, EventDataSet[] changes, UnityAction done = null) {
            lastIndex = -1;
            changedStoreData = eventdata;
            changingData = changes;
            changeDoneHandler = done;
            ChangeProcLoop();
        }

        /// <summary>
        /// データを変更した時に登録されている処理を全て実行。
        /// </summary>
        public void ChangeProcLoop() {
            int idx = IndexOfCheckData(changedStoreData, lastIndex+1, changingData);
            DebugLog("idx=" + idx + " / last=" + lastIndex);
            if (idx==-1) {
                changeDoneHandler?.Invoke();
                return;
            }

            lastIndex = idx;

            DebugLog(name + ".Change Proc Loop : "+eventProcData[idx].checkData);
            eventProcData[idx].onChange?.Invoke();
            if (eventProcData[idx].onChangeSE != SoundPlayer.SE.none) {
                SoundPlayer.PlaySE(eventProcData[idx].onChangeSE);
            }
        }

        /// <summary>
        /// 変更するデータセットがchangesで渡されるので、
        /// このオブジェクトに設定されているcheckData配列のうち、
        /// 該当するものがあるかを検索して、見つけたらインデックスを返します。
        /// </summary>
        /// <param name="store">現在の状態</param>
        /// <param name="changes">変更したデータセット。未設定の時は、現在のstoreの状態をそのまま反映</param>
        /// <returns>見つけた場合はそのインデックス。見つからなかったら-1</returns>
        public int IndexOfCheckData(EventDataSet[] store, int start, EventDataSet[] changes = null) {
            for (int i = start ; i < eventProcData.Length; i++) {
                if (EventDataSet.CheckDataIn(
                    eventProcData[i].checkData.EventDataSets,
                    store,
                    changes)) {
                    return i;
                }
            }

            return -1;
        }

        /// <summary>
        /// アニメーション完了時に処理するデフォルト処理。アニメーションを完了させて、登録されている処理を呼び出します。
        /// </summary>
        public void OnAnimeDone() {
            AnimeDone();
            ChangeProcLoop();
        }

        /// <summary>
        /// 変更処理を呼び出した条件データのうち、指定のvalue値を持つ要素を返します。
        /// </summary>
        /// <param name="val">探したいvalueの文字列</param>
        /// <returns>見つけたEventDataSetインスタンスを返します。見つからないときはnull</returns>
        public EventDataSet GetEventDataSetWithValue(string val) {
            EventDataSet[] chg = eventProcData[lastIndex].checkData.EventDataSets;
            for (int i=0; i<chg.Length;i++) {
                if (chg[i].value == val) {
                    return chg[i];
                }
            }
            return null;
        }

        #endregion EventProcedure

        #region Event Services

        /// <summary>
        /// EventProcDataに設定するメソッド。アニメーションの開始と、完了時の呼び出しを行います。
        /// これで開始するアニメーションは、終了時に`OnAnimeDone()`を呼び出すこと。
        /// </summary>
        /// <param name="animName"></param>
        public void SetAnimeTrue(string animName) {
            if ((animName == animatorParamName) && animatorFlag) {
                DebugLog("アニメ状態が同じ " + animName + " = True");
                ChangeProcLoop();
                return;
            }

            animatorParamName = animName;
            animatorFlag = true;
            DebugLog(animName + " to True : "+anim);
            AnimeStart();
            anim?.SetBool(animName, true);
        }

        public void SetAnimeFalse(string animName) {
            if ((animName == animatorParamName) && !animatorFlag) {
                DebugLog("アニメ状態が同じ " + animName + " = False");
                ChangeProcLoop();
                return;
            }

            animatorParamName = animName;
            animatorFlag = false;

            DebugLog(animName + " to False : "+anim);
            AnimeStart();
            anim?.SetBool(animName, false);
        }

        public void SetAnimeTrigger(string anime) {
            DebugLog(anime + " Trigger");
            AnimeStart();
            anim?.SetTrigger(anime);
        }

        /// <summary>
        /// 指定のメッセージを表示します。
        /// メッセージ表示中は、メッセージ側で進行を停止するので、こちらの処理は進めます。
        /// </summary>
        /// <param name="mesname"></param>
        public void ShowMessage(string mesname) {
            MessageManager.Instance.ShowMessage(MessageManager.Instance.GetMessage(mesname));
            ChangeProcLoop();
        }

        #endregion Event Services

        [System.Diagnostics.Conditional("EVENTINVOKER_DEBUG")]
        static void DebugLog(object msg) {
            Debug.Log(msg);
        }
    }

}
