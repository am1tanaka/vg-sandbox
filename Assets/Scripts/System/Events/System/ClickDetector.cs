﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using AM1.Nav;
using UnityEngine.EventSystems;

namespace VG {
    public class ClickDetector : MonoBehaviour {

        [Tooltip("クリックした時に向かう場所。このオブジェクトからの相対座標。"), SerializeField]
        public Vector3 targetPosition;
        [Tooltip("向かう場所をアニメーションによらずに固定したい時にチェックする。"), SerializeField]
        public bool isFixedTargetPosition;
        [Tooltip("到着時に向く場所。このオブジェクトからの相対座標。"), SerializeField]
        public Vector3 lookPosition;
        [Tooltip("見る先をアニメーションによらずに固定したい時にチェックする。"), SerializeField]
        public bool isFixedLookPosition;
        [Tooltip("クリックを知らせる相手。未設定だと自動的にPlayerへ。"), SerializeField]
        NavController sendObject;
        [Tooltip("クリックして発動する時の条件や設定する値などのデータ。VG/Create Detector Dataで作成したアセットを必要な数だけ設定します。"), SerializeField]
        public DetectorData[] detectorData;

        /// <summary>
        /// 検出したインデックス
        /// </summary>
        public int detectIndex { get; private set; }

        /// <summary>
        /// クリックできるフラグ。isDataChangingとisPlayerAnimationがどちらもfalseになったらtrueになります。
        /// </summary>
        static bool canClick {
            get {
                return !EventSequencer.isRunning;
            }
        }

        /// <summary>
        /// テスト用データ
        /// </summary>
        static DetectorData debugData = null;

        public Vector3 startPosition { get; private set; }
        DetectorData nowProcDetectorData;

        private void Awake() {
            startPosition = transform.position;
            if (sendObject == null) {
                GameObject go = GameObject.FindGameObjectWithTag("Player");
                if (go != null) {
                    sendObject = go.GetComponent<NavController>();
                }
                else {
                    Debug.Log(name + " : Send Objectを設定するか、Playerタグのオブジェクトを作成してください。");
                }
            }
            nowProcDetectorData = null;

            if (debugData == null) {
                debugData = ScriptableObject.CreateInstance<DetectorData>();
                debugData.actionName = "";
                debugData.changeDataSet = new EventDataSet[0];
                debugData.triggerRules = new EventDataSet[0];
            }
            if ((detectorData == null) || (detectorData.Length == 0)) {
                detectorData = new DetectorData[1];
                detectorData[0] = debugData;
            }
        }

        private void OnMouseDown() {
            // 処理中や、UIが操作不能な時、マウスがUI上の時は何もしない
            if (    !canClick
                ||  !UIManager.isClickDetectable
                ||  GameSystem.IsOnUI(Input.mousePosition)) {
#if UNITY_EDITOR
                Debug.Log("Cant Emmit : "+" / isPlayerAnimation="+ "isAnimeDone="+ sendObject.isAnimeDone + " / detectable=" + UIManager.isClickDetectable+" / IsOnUI="+ GameSystem.IsOnUI(Input.mousePosition));

                foreach(RaycastResult res in GameSystem.raycastResults) {
                    Debug.Log(res.gameObject.name);
                }
#endif
                return;
            }

            // データがない場合はテスト起動
            detectIndex = -1;

            // 発動イベントがあるかを確認
            for (int i = 0; i < detectorData.Length; i++) {
                // ルールが0の場合は無条件発動
                if (detectorData[i].triggerRules.Length == 0) {
                    detectIndex = i;
                    break;
                }
                // ルールをチェック
                if (DataStore.Instance.Check(detectorData[i].triggerRules)) {
                    detectIndex = i;
                    break;
                }
            }

            if (detectIndex == -1) {
                // 見つからなかったら発動なし
                return;
            }

            StartCoroutine(EventSequencer.Instance.EventSequence(this, sendObject != null));
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected() {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position + targetPosition, 0.1f);

            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position + lookPosition, 0.09f);
        }
#endif
    }
}
