﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace VG {
    /// <summary>
    /// 実際のデータの保持、変更、読み書きをするクラス
    /// </summary>
    public class DataStore : MonoBehaviour {
        public static DataStore Instance {
            get;
            private set;
        }

        /// <summary>
        /// 初期データ
        /// </summary>
        [Header("ゲーム開始時の全設定")]
        [Tooltip("この面で利用するすべてのデータの最初の設定のEventDataを設定します。"), SerializeField]
        EventData initData;

        [Tooltip("データの変更に応じた処理を登録しているEventInvokerをアタッチしたオブジェクトをここにすべて登録します。"), SerializeField]
        EventInvoker[] eventInvokers;

        /// <summary>
        /// データストア
        /// </summary>
        EventDataSet[] store;

        /// <summary>
        /// 変更中のデータセット。イベントの発動条件にこの変更が含まれていない場合は、イベントを発動させないようにするために利用。
        /// </summary>
        EventDataSet[] changeData;

        private void Awake() {
            Instance = this;
        }

        private void Start() {
            // ストアを初期化
            if (store == null) {
                store = new EventDataSet[initData.EventDataSets.Length];
            }
            // パラメーター名と値を設定
            for (int i = 0; i < initData.EventDataSets.Length; i++) {
                store[i] = new EventDataSet(initData.EventDataSets[i]);
            }

            // 設定処理を呼び出す
            for (int i=0;i<eventInvokers.Length;i++) {
                eventInvokers[i].SetProc(store);
            }
        }

#if UNITY_EDITOR
        private void Update() {
            if (Input.GetKeyDown(KeyCode.D)) {
                for (int i = 0; i < store.Length; i++) {
                    Debug.Log("store[" + i + "]=" + store[i].name + " = " + store[i].value);
                }
                if (changeData != null) {
                    for (int i = 0; i < changeData.Length; i++) {
                        Debug.Log("change[" + i + "]=" + changeData[i].name + " = " + changeData[i].value);
                    }
                }
            }
        }
#endif

        /// <summary>
        /// 新しく設定するデータを反映させて、イベント処理のSetをします。
        /// </summary>
        /// <param name="change">設定するデータリスト</param>
        public void Set(EventDataSet[] change) {
            EventDataSet.ChangeData(store, change);
            for (int i = 0; i < eventInvokers.Length; i++) {
                eventInvokers[i].SetProc(store);
            }
        }

            /// <summary>
            /// まずはchangesで渡されるデータ配列を使ってstoreを書き換える。
            /// あとは、eventProcsに設定されているイベントの条件をチェックして、
            /// changesで行った変更に関連するeventProcsのイベントを発動させる。
            /// イベントはeventProcsのデータをひとまとめとして行い、シーケンシャルに実行していく。
            /// </summary>
            /// <param name="changes">変更するデータ配列</param>
            /// <param name="done">完了時に呼び出すコールバックメソッド</param>
            public void Change(EventDataSet [] changes, UnityAction done) {
            EventDataSet.ChangeData(store, changes);
            changeIndex = 0;
            changeDoneHandler = done;
            changeData = changes;
            changeLoop();
        }

        int changeIndex;
        UnityAction changeDoneHandler;

        /// <summary>
        /// パラメーターを変更するループ処理。処理が完了したら、changeLoopDoneを呼び出させて、値を更新する。
        /// 更新がなくなったら、完了処理を呼び出すためにchangeDoneHandlerを実行する。
        /// </summary>
        void changeLoop() {
            if (changeIndex >= eventInvokers.Length) {
                changeDoneHandler?.Invoke();
                return;
            }

            eventInvokers[changeIndex].ChangeProc(store, changeData, changeLoopDone);
        }

        void changeLoopDone() {
            changeIndex++;
            changeLoop();
        }

        /// <summary>
        /// changeに登録されているデータセットに合わせて、
        /// storeを変更します。
        /// </summary>
        /// <param name="change">変更するデータセット</param>
        private void changeEventData(EventDataSet[] change) {
            EventDataSet.ChangeData(store, change);
        }

        /// <summary>
        /// checkで指定した条件が、現在の状態に合致する場合、trueを返します。
        /// </summary>
        /// <param name="check">チェックしたいデータのセット</param>
        /// <returns>checkがすべて満たされていたらtrueを返します。</returns>
        public bool Check(EventDataSet[] check) {
            return EventDataSet.CheckDataIn(check, store);
        }

    }
}
