﻿/*
* MIT License
* Copyright (c) 2018 Yu Tanaka
* https://github.com/am1tanaka/OpenSimpleFramework201801/blob/master/LICENSE
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

/// <summary>
/// BGMやSEを制御するクラスです。
/// </summary>
public class SoundPlayer : MonoBehaviour {
    /// <summary>
    /// 自分のインスタンス
    /// </summary>
    public static SoundPlayer Instance {
        get;
        private set;
    }

    [TooltipAttribute("BGM用のオーディオソース"), SerializeField]
    private AudioSource audioBGM;
    [TooltipAttribute("SE用のオーディオソース"), SerializeField]
    private AudioSource audioSE;

    /// <summary>
    /// BGMの列挙子。
    /// </summary>
    [System.Serializable]
    public enum BGM {
        GAME,
        NONE    // 最後にNONEを置いてください
    }
    [TooltipAttribute("BGMリスト。並び順は内部で自動的に調整するので、片っ端から放り込んで大丈夫です。"), SerializeField]
    private AudioClip[] BGMList;

    /// <summary>
    /// 効果音リスト。
    /// </summary>
    [System.Serializable]
    public enum SE {
        none,
        ui_get_item,
        ui_typing,
        ui_page,
        ui_menu_open,
        ui_menu_close,
        ui_menu_selectitem,
        se_curtain_open,
        se_curtain_close,
        se_light_on,
        se_light_off,
        se_door_turned_hundle,
        se_door_open,
        se_move_cushion,
        se_camera_turn,
        se_camera_zoomin,
        se_camera_zoomout,
        se_hint_mysterious,
        se_count // これを最後に定義
    };
    [TooltipAttribute("効果音リスト。並び順は内部で自動的に調整するので、片っ端から放り込んで大丈夫です。"), SerializeField]
    private AudioClip[] SEList;

    Dictionary<BGM, AudioClip> bgmDictionary = new Dictionary<BGM, AudioClip>();
    Dictionary<SE, AudioClip> seDictionary = new Dictionary<SE, AudioClip>();

    void Awake() {
        if (Instance == null) {
            Instance = this;

            for (int i=0;i<(int)SE.se_count;i++) {
                string key = ((SE)i).ToString();
                for (int j=0; j<SEList.Length;j++) {
                    if (SEList[j].name == key) {
                        seDictionary.Add((SE)i, SEList[j]);
                        break;
                    }
                }
            }

            /*
            foreach (AudioClip bgm in BGMList) {
                bgmDictionary.Add(bgm.name, bgm);
            }
            */
        }
    }

    /// <summary>
    /// 指定の効果音を鳴らします。
    /// </summary>
    /// <param name="snd">再生したい効果音</param>
    public static void PlaySE(SE snd) {
        Instance.audioSE.PlayOneShot(Instance.seDictionary[snd]);
    }

    /// <summary>
    /// 指定のBGMを再生します。
    /// </summary>
    /// <param name="bgm">再生したいBGM</param>
    public static void PlayBGM(BGM bgm) {
        Instance.playBGM(bgm);
    }

    private void playBGM(BGM bgm) {
        // 同じ曲が設定されていて再生中ならなにもしない
        if (audioBGM.clip == BGMList[(int)bgm]) {
            if (audioBGM.isPlaying) {
                return;
            }
        }
        else {
            // 違う曲の場合
            // 曲が設定されていたら、曲を停止
            if (audioBGM.clip != null) {
                audioBGM.Stop();
            }

            // 曲を設定
            audioBGM.clip = Instance.BGMList[(int)bgm];
        }

        // 再生開始
        audioBGM.Play();
    }

    /// <summary>
    /// BGMを停止します。
    /// </summary>
    public static void StopBGM(bool isFadeOut = false) {
        if ((Instance.audioBGM.clip != null) && (Instance.audioBGM.isPlaying)) {
            Instance.audioBGM.Stop();
        }
    }

    /// <summary>
    /// BGMのボリュームを設定します。
    /// </summary>
    /// <param name="vol">0=消音 / 1=最大ボリューム</param>
    public static void SetBGMVolume(float vol) {
        Instance.audioBGM.volume = vol;
    }

    /// <summary>
    /// 効果音を停止します。
    /// </summary>
    public static void StopSE() {
        Instance.audioSE.Stop();
    }
}

