﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimeDoneHandler : MonoBehaviour {

    [Tooltip("完了時に呼び出すイベント"), SerializeField]
    UnityEvent animDone;

    public void AnimeDone() {
        animDone?.Invoke();
    }

}
