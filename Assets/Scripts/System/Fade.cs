﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(GameSystem))]
public class Fade : MonoBehaviour {

    public enum FADE_STATE {
        NONE,
        IN,
        OUT
    }

    /// <summary>
    /// フェードイン・アウト中の時、trueを返します。
    /// </summary>
    public static FADE_STATE FadeState {
        get;
        private set;
    }

    /// <summary>
    /// フェードインが完了した時に実行したいメソッドを登録するためのハンドラー。
    /// </summary>
    private UnityEvent onPostFadeIn;

    /// <summary>
    /// フェードアウトが完了した時に実行したいメソッドを登録するためのハンドラー。
    /// </summary>
    private UnityEvent onPostFadeOut;

    [TooltipAttribute("フェードアニメーター"), SerializeField]
    private Animator fadeAnimator;

    private bool isFading;

    /// <summary>
    /// フェードアウトが完了した時に、アニメーションから呼ばれます。
    /// </summary>
    public void OnFadeOutDone() {
        FadeState = FADE_STATE.NONE;
        if (onPostFadeOut != null) {
            onPostFadeOut.Invoke();
        }
    }

    /// <summary>
    /// フェードインを開始します。
    /// </summary>
    public IEnumerator FadeIn() {
        FadeState = FADE_STATE.IN;
        fadeAnimator.SetTrigger("FadeIn");
        while (FadeState == FADE_STATE.IN) {
            yield return null;
        }
    }

    /// <summary>
    /// フェードアウトを実行します。
    /// </summary>
    public IEnumerator FadeOut() {
        FadeState = FADE_STATE.OUT;
        fadeAnimator.SetTrigger("FadeOut");
        while (FadeState == FADE_STATE.OUT) {
            yield return null;
        }
    }

    /// <summary>
    /// フェードインが完了した時に、アニメーションから呼ばれます。
    /// </summary>
    public void OnFadeInDone() {
        FadeState = FADE_STATE.NONE;
        if (onPostFadeIn != null) {
            onPostFadeIn.Invoke();
        }
    }

    /// <summary>
    /// フェードイン後に実行したい処理を登録します。
    /// </summary>
    /// <param name="postFadeInMethod">引数、戻り値なしのメソッドを渡します。</param>
    public void SetPostFadeIn(UnityAction postFadeInMethod) {
        onPostFadeIn.AddListener(postFadeInMethod);
    }

    /// <summary>
    /// フェードアウト後に実行したい処理を登録します。
    /// </summary>
    /// <param name="postFadeInMethod">引数、戻り値なしのメソッドを渡します。</param>
    public void SetPostFadeOut(UnityAction postFadeOutMethod) {
        onPostFadeOut.AddListener(postFadeOutMethod);
    }
}
