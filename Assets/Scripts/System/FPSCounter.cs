﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AM1
{

    public class FPSCounter : MonoBehaviour
    {
        public static FPSCounter Instance
        {
            get;
            private set;
        }

        [TooltipAttribute("FPSを表示するか。ビルドしたら自動的に消えます。"), SerializeField]
        private bool isDebug = true;

        public static int FPS
        {
            get
            {
                if (Mathf.Approximately(averagedDeltaTime, 0f))
                {
                    return -1;
                }
                return (int)(1f / averagedDeltaTime + 0.5f);
            }
        }
        private static float averagedDeltaTime;
        public static float MaxDeltaTime
        {
            get;
            private set;
        }
        private static int startFrameCount;

        private void Awake()
        {
            Instance = this;
        }

        // Use this for initialization
        void Start()
        {
            averagedDeltaTime = 1f / 60f;
            ResetFPS();
        }

        public static void ResetFPS()
        {
            MaxDeltaTime = 0f;
            startFrameCount = Time.frameCount;
        }

        // Update is called once per frame
        void Update()
        {
            float past = (float)(Time.frameCount - startFrameCount);
            if (Mathf.Approximately(past, 0f))
            {
                past = 1f;
            }
            averagedDeltaTime = (averagedDeltaTime * (past - 1f)) / past + Time.deltaTime / past;
            MaxDeltaTime = Mathf.Max(MaxDeltaTime, Time.deltaTime);
        }

#if UNITY_EDITOR
        private void OnGUI()
        {
            if (isDebug)
            {
                float dt = Mathf.Approximately(averagedDeltaTime, 0f) ? 1f : averagedDeltaTime;
                GUI.color = Color.red;
                GUI.Label(new Rect(10, 10, 100, 30), "" + (int)(1f/dt+0.5f)+" FPS");
            }
        }
#endif
    }
}
