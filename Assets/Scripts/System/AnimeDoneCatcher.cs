﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimeDoneCatcher : MonoBehaviour {

    /// <summary>
    /// 動作完了フラグ
    /// </summary>
    public bool isDone { get; private set; }
    /// <summary>
    /// 次のアクションに進んでよいことを示すフラグ。
    /// 探すアクションなどで呼び出し
    /// </summary>
    public bool isNextAction { get; private set; }

    /// <summary>
    /// フラグを初期化します
    /// </summary>
    public void ClearFlags() {
        isDone = false;
        isNextAction = false;
    }

    /// <summary>
    /// アニメ終了時のイベントを発動するための機能
    /// </summary>
    public void AnimeDone() {
        isDone = true;
    }

    /// <summary>
    /// 次の行動に移行してよいタイミングを知らせるために、アニメーションから呼び出します。
    /// </summary>
    public void NextAction() {
        isNextAction = true;
    }


}
