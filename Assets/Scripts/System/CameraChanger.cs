﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChanger : MonoBehaviour {
    [TooltipAttribute("カメラの位置を、手前、左、奥、右の順に設定する"), SerializeField]
    Transform[] cameraPos;
    [TooltipAttribute("カメラの移動レート"), SerializeField, Range(0,1)]
    float moveRate = 0.1f;
    [Tooltip("カメラの移動最高速度"), SerializeField]
    float speedMax = 4f;

    public static CameraChanger Instance {
        get;
        private set;
    }

    /// <summary>
    /// 現在のカメラの位置。左クリックでインクリメント。右クリックでデクリメント
    /// </summary>
    int cameraPosIndex;

    /// <summary>
    /// 移動先のカメラ情報
    /// </summary>
    Camera targetCamera;

    private void Awake() {
        Instance = this;
    }

    void Start () {
        cameraPosIndex = 0;
        Camera.main.transform.SetPositionAndRotation(cameraPos[cameraPosIndex].position, cameraPos[cameraPosIndex].rotation);
        AddCameraIndex(0);

        // カメラを無効化
        for (int i=0; i<cameraPos.Length;i++) {
            Camera cam = cameraPos[i].gameObject.GetComponentInChildren<Camera>();
            cam.enabled = false;
        }
	}
	
	void LateUpdate () {
		if (!GameSystem.IsControllable) {
            return;
        }

        // カメラの調整
        Vector3 frompos = Camera.main.transform.position;
        Vector3 targetpos = targetCamera.transform.position;
        Vector3 nextpos = Vector3.Lerp(frompos, targetpos, moveRate);
        Vector3 todir = nextpos - frompos;
        if (Mathf.Approximately(todir.magnitude, 0f)) {
            return;
        }

        float speed = speedMax * Time.deltaTime;
        if (todir.magnitude > speed) {
            nextpos = frompos + todir.normalized * speed;
        }
        else {
            speed = todir.magnitude;
        }

        float t = speed / (targetpos-frompos).magnitude;
        float newfov = Mathf.Lerp(Camera.main.fieldOfView, targetCamera.fieldOfView, t);
        Quaternion newrot = Quaternion.Slerp(Camera.main.transform.rotation, targetCamera.transform.rotation, t);

        Camera.main.transform.SetPositionAndRotation(nextpos, newrot);
        Camera.main.fieldOfView = newfov;
    }

    /// <summary>
    /// カメラの切り替え
    /// </summary>
    /// <param name="add">-1か1</param>
    public void AddCameraIndex(int add) {
        cameraPosIndex = (int)Mathf.Repeat(cameraPosIndex + add, cameraPos.Length);
        targetCamera = cameraPos[cameraPosIndex].gameObject.GetComponent<Camera>();
        SoundPlayer.PlaySE(SoundPlayer.SE.se_camera_turn);
    }

    /// <summary>
    /// 一時的にカメラを切り替えたい時に、切り替えたいカメラのオブジェクトを渡す。
    /// 切り替えが終わったら、AddCameraIndex(0)で元のカメラに戻す。
    /// </summary>
    /// <param name="cam">カメラ</param>
    public void SetTargetCamera(Camera cam) {
        targetCamera = cam;
    }
}
