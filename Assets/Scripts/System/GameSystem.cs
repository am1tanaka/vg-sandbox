﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

/// <summary>
/// シーンの切り替えやフェード処理を管理するクラス
/// </summary>
[RequireComponent(typeof(Fade))]
public class GameSystem : Singleton<GameSystem> {
#pragma warning disable 0414
    [TooltipAttribute("開始シーン名"), SerializeField]
    string startScene = "Title";
#pragma warning restore 0414
    [TooltipAttribute("ゲームシステムの設定")]
    public GameSettings Settings;

    /// <summary>
    /// プレイヤーなどの操作が可能かを返します。シーン切り替え中は操作不能なのでfalseを返します。
    /// </summary>
    public static bool IsControllable {
        get {
            return !Instance.isSceneChanging;
        }
    }

    /// <summary>
    /// このオブジェクトにアタッチされているフェードクラスのインスタンスをとっておくやつ。
    /// </summary>
    private static Fade fade;

    /// <summary>
    /// シーン切り替え予定のシーン名
    /// </summary>
    private static string nextSceneName = "";

    /// <summary>
    /// シーンチェンジ中
    /// </summary>
    private bool isSceneChanging;

    //---- コード

    protected override void Awake()
    {
        base.Awake();

        isSceneChanging = false;

        if (Settings == null)
        {
            LogError("SettingsにGameSettingsを設定してください");
            Application.Quit();
            return;
        }

        if (Instance == this) {
            fade = GetComponent<Fade>();

#if !UNITY_EDITOR
            // ビルド時はタイトルシーンを読み込む
            SceneManager.LoadScene(startScene, LoadSceneMode.Additive);
#endif
        }
    }

    /// <summary>
    /// フェードインが完了した時に実行したい処理を設定します。
    /// 設定できるのは引数、戻り値なしのメソッドです。
    /// </summary>
    /// <param name="postFadeInMethod">引数、戻り値なしのメソッド</param>
    public static void SetPostFadeIn(UnityAction postFadeInMethod) {
        fade.SetPostFadeIn(postFadeInMethod);
    }

    /// <summary>
    /// フェードアウトが完了した時に実行したい処理を設定します。
    /// 設定できるのは引数、戻り値なしのメソッドです。
    /// </summary>
    /// <param name="postFadeInMethod">引数、戻り値なしのメソッド</param>
    public static void SetPostFadeOut(UnityAction postFadeInMethod) {
        fade.SetPostFadeOut(postFadeInMethod);
    }

    /// <summary>
    /// Systemシーンは残して、シーンを切り替えます。
    /// フェードアウト後やフェードイン後に実行したい処理があったら、
    /// SetPostFadeIn()やSetPostFadeOut()でメソッドを登録しておきます。
    /// フェードアウト中は、予約中の次シーンを書き換えるだけにします。
    /// フェードイン中は、フェードインが完了した後に
    /// </summary>
    /// <param name="scenename"></param>
    public static void ChangeScene(string nextScene) {
        nextSceneName = nextScene;

        // フェードアウト時なら現在動いているコルーチンに任せる
        if (Instance.isSceneChanging) {
            return;
        }

        // フェードアウトとシーンの切り替え用コルーチン
        Instance.StartCoroutine(Instance.sceneChangeProc());
    }

    private IEnumerator sceneChangeProc() {
        isSceneChanging = true;
        do {
            // フェードアウト
            yield return fade.FadeOut();

            // フェードアウト完了。Systemシーン以外解放します
            // 登録済みの処理はFade側で自動的に処理されるのでここでは処理は不要
            for (int i=0; i< SceneManager.sceneCountInBuildSettings; i++) {
                Scene sc = SceneManager.GetSceneByBuildIndex(i);
                if (sc.IsValid() && sc.name != "System") {
                    yield return SceneManager.UnloadSceneAsync(sc);
                }
            }

            // シーンを読み込みます
            SceneManager.LoadScene(nextSceneName, LoadSceneMode.Additive);
            nextSceneName = "";

            // 読み込みが完了したらフェードインを実行します
            yield return fade.FadeIn();
        } while (nextSceneName.Length > 0);
        isSceneChanging = false;
    }

    /// <summary>
    /// UnityEditorで実行時のみ、コンソールにエラー出力します。
    /// </summary>
    /// <param name="mes">出力したい内容</param>
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    public static void LogError(object mes)
    {
        Debug.LogError(mes);
    }

    /// <summary>
    /// UnityEditorで実行時のみ、コンソール出力します。
    /// </summary>
    /// <param name="mes">出力したい内容</param>
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    public static void Log(object mes)
    {
        Debug.Log(mes);
    }

    /// <summary>
    /// UIに重なっていない状態の時の指定のマウスボタンの押されている状態を返します。
    /// </summary>
    /// <param name="num">チェックしたいマウスボタンの番号</param>
    /// <returns>指定のボタン番号のマウスがクリックされた時true。ただしマウスがUI上の時はfalse</returns>
    public static bool GetMouseButtonDown(int num) {
        if (!Input.GetMouseButtonDown(num)) return false;

        return !IsOnUI(Input.mousePosition);
    }

    #region Methods

    static PointerEventData pointerEventData = null;
    public static List<RaycastResult> raycastResults { get; private set; }

    /// <summary>
    /// 指定のスクリーン座標がUIを指している場合にtrueを返します。
    /// raycastResultsを確認すれば、重なっているUIの情報を確認できます。
    /// </summary>
    /// <param name="spos">スクリーン座標</param>
    /// <returns>UIがある時、true</returns>
    public static bool IsOnUI(Vector3 spos) {
        if (pointerEventData == null) {
            pointerEventData = new PointerEventData(EventSystem.current);
        }
        pointerEventData.position = spos;
        if (raycastResults == null) {
            raycastResults = new List<RaycastResult>();
        }
        else {
            raycastResults.Clear();
        }
        EventSystem.current.RaycastAll(pointerEventData, raycastResults);
        return raycastResults.Count > 0;
    }

    #endregion

}
