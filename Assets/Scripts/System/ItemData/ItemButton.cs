﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace VG {
    public class ItemButton : MonoBehaviour {

        ItemMaster itemMaster;

        /// <summary>
        /// ボタンにアイテムのマスターデータを設定して、画像を設定します。
        /// </summary>
        /// <param name="im">アイテムマスター</param>
        public void SetItemMaster(ItemMaster im) {
            itemMaster = im;
            transform.GetChild(0).GetComponent<Image>().sprite = im.menuImage;
        }

        public void Click() {
            SoundPlayer.PlaySE(SoundPlayer.SE.ui_menu_selectitem);
            ItemMenu.Instance.Select(itemMaster.type);
        }

    }
}
