﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Specialized;

namespace VG {
    public class ItemListManager : MonoBehaviour {
        public static ItemListManager Instance {
            get;
            private set;
        }

        #region Defines

        [Tooltip("アイテムマスター"), SerializeField]
        List<ItemMaster> itemMasterList;

        /// <summary>
        /// 実際に扱うアイテムマスターの参照元データ。itemMasterListから生成します。
        /// </summary>
        public static ReadOnlyDictionary<ItemMaster.ItemType, ItemMaster> itemMaster { get; private set; }

        /// <summary>
        /// アイテムを持てる最大数
        /// </summary>
        public const int ITEM_MAX = 8;

        /// <summary>
        /// 現在持っているアイテムのリスト。
        /// </summary>
        public ObservableCollection<ItemMaster> itemBank {
            get;
            private set;
        }

        /// <summary>
        /// 現在、使用中のアイテム名。未指定の時は0文字。
        /// </summary>
        public string usingItemName {
            get;
            private set;
        }

        #endregion Defines

        #region System

        private void Awake() {
            Instance = this;
            itemMaster = new ReadOnlyDictionary<ItemMaster.ItemType, ItemMaster>(itemMasterList.ToDictionary(ItemMaster => ItemMaster.type));
            itemBank = new ObservableCollection<ItemMaster>();
            itemBank.CollectionChanged += ItemMenu.OnCollectionChanged;
            usingItemName = "";
        }

        #endregion System

        #region Methods

        /// <summary>
        /// 指定のDataNameに対応するアイテムのタイプを返します。
        /// </summary>
        /// <param name="dataName">探したいEventDataSetのDATA_NAME</param>
        /// <returns>該当するアイテムのタイプ</returns>
        public ItemMaster.ItemType GetItemType(EventDataSet.DATA_NAME dataName) {
            ItemMaster im = itemMasterList.Find(x => x.itemDataName == dataName);
            if (im == null) {
                return ItemMaster.ItemType.None;
            }

            return im.type;
        }

        /// <summary>
        /// 指定の名前のアイテムを入手します。
        /// </summary>
        /// <param name="type">ItemMasterのname</param>
        public void GetItem(ItemMaster.ItemType type) {
            if (!itemMaster.ContainsKey(type)) {
#if UNITY_EDITOR
                Debug.LogError(type + "をItem Mastersに登録してください。");
#endif
                return;
            }

            // 指定のアイテムを持っているか
            ItemMaster im = itemMaster[type];

            // すでに持っていたら何もしない
            if (itemBank.Contains(im)) {
#if UNITY_EDITOR
                Debug.Log(type + "はすでに持っています。");
#endif
                return;
            }

            itemBank.Add(im);
        }

        /// <summary>
        /// 指定のアイテムをアイテムリストから削除します。
        /// </summary>
        /// <param name="type">削除したいアイテムタイプ</param>
        public void ReleaseItem(ItemMaster.ItemType type) {
            itemBank.Remove(itemMaster[type]);
        }

        #endregion Methods

    }
}
