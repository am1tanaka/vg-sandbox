﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VG {
    [CreateAssetMenu(menuName = "VG/Item Master")]
    public class ItemMaster : ScriptableObject {
        /// <summary>
        /// アイテムの種類。ここにすべてのアイテムを定義します。
        /// </summary>
        public enum ItemType {
            Kami,
            RedBox,
            Key,
            None
        }

        /// <summary>
        /// アイテムの種類
        /// </summary>
        public ItemType type;

        /// <summary>
        /// アイテム名
        /// </summary>
        public string itemName;

        /// <summary>
        /// このアイテムに対応するEventDataSetのDATA_NAME
        /// </summary>
        public EventDataSet.DATA_NAME itemDataName;

        /// <summary>
        /// アイテム画面に表示する画像
        /// </summary>
        public Sprite menuImage;

        /// <summary>
        /// アイテムの3Dモデルプレハブ
        /// </summary>
        public GameObject prefab;
    }
}
