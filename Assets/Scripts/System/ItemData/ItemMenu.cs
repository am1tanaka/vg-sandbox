﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.UI;

namespace VG {
    public class ItemMenu : MonoBehaviour {

        public static ItemMenu Instance {
            get;
            private set;
        }

        /// <summary>
        /// 表示しているアイテムのボタンを記録するための構造体
        /// </summary>
        struct ItemButtonData {
            public GameObject buttonObject;
            public ItemMaster itemMaster;
        }

        #region SerializeFields

        [Tooltip("ボタンを設置する親オブジェクト"), SerializeField]
        Transform itemParent;
        [Tooltip("ボタンプレハブ"), SerializeField]
        GameObject buttonPrefab;
        [Tooltip("選択アイコン"), SerializeField]
        GameObject selectObject;

        #endregion SerializeFields

        #region Variables

        /// <summary>
        /// 表示しているアイテムボタンのリスト
        /// </summary>
        List<ItemButtonData> itemButtons = new List<ItemButtonData>();

        /// <summary>
        /// 現在選択しているアイテムのタイプ。未選択の時は
        /// </summary>
        ItemMaster.ItemType selectedItem = ItemMaster.ItemType.None;

        Animator anim;
        bool isOpened = false;

        #endregion Variables

        #region System

        private void Awake() {
            Instance = this;
            anim = GetComponent<Animator>();
            anim.SetBool("Open", isOpened);
        }

        public static void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
            switch (e.Action) {
                case NotifyCollectionChangedAction.Add:
                    Instance.addItem(e.NewItems);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    Instance.removeItem(e.OldItems);
                    break;
                default:
#if UNITY_EDITOR
                    Debug.LogWarning("実装していない処理：" + e.Action);
#endif
                    break;
            }
        }

        #endregion System

        #region Public Methods

        /// <summary>
        /// パネルの開閉をトグル操作します。
        /// </summary>
        public void TogglePanelOpen() {
            isOpened = !isOpened;
            anim.SetBool("Open", isOpened);
            SoundPlayer.PlaySE(isOpened ? SoundPlayer.SE.ui_menu_open : SoundPlayer.SE.ui_menu_close);
        }

        /// <summary>
        /// アイテムを選択します。
        /// </summary>
        /// <param name="type">選択するアイテム</param>
        public void Select(ItemMaster.ItemType type) {
            selectedItem = type;
            updateSelectCursor();
        }

        #endregion Public Methods


        #region Item操作

        /// <summary>
        /// 渡されたリストのアイテムを保有します。
        /// </summary>
        /// <param name="adds"></param>
        void addItem(IList adds) {
            ItemButtonData addData;

            for (int i = 0; i < adds.Count; i++) {
                ItemMaster itemmaster = (ItemMaster)adds[i];
                // 追加済みなら何もしない
                if (itemButtons.Exists(
                    im => im.itemMaster.type == itemmaster.type
                    )) {
#if UNITY_EDITOR
                    Debug.LogWarning(itemmaster.type + "は追加済み");
#endif
                    continue;
                }

                // 追加
                GameObject go = Instantiate<GameObject>(buttonPrefab, itemParent);
                ItemButton ib = go.GetComponent<ItemButton>();
                ib.SetItemMaster(itemmaster);
                addData.buttonObject = go;
                addData.itemMaster = itemmaster;
                itemButtons.Add(addData);
            }

            // ボタンを並べる
            layoutButtons();
        }

        void removeItem(IList items) {
            ItemButtonData removeData;

            for (int i = 0; i < items.Count; i++) {
                ItemMaster itemmaster = (ItemMaster)items[i];
                removeData = itemButtons.Find(
                    ib => ib.itemMaster.type == itemmaster.type
                );
                itemButtons.Remove(removeData);
                Destroy(removeData.buttonObject);
                removeData.itemMaster = null;
            }

            // ボタンを並べる
            layoutButtons();
        }

        #endregion Item操作

        /// <summary>
        /// 登録されているボタンをアイテムメニューに並べます。
        /// </summary>
        void layoutButtons() {
            Vector3 pos = Vector3.zero;
            float w = buttonPrefab.GetComponent<RectTransform>().rect.width;
            pos.x = itemButtons.Count * w * -0.5f;
            for (int i = 0; i < itemButtons.Count; i++, pos.x += w) {
                itemButtons[i].buttonObject.transform.localPosition = pos;
            }

            updateSelectCursor();
        }

        /// <summary>
        /// 選択カーソルを現在の状態に更新
        /// </summary>
        void updateSelectCursor() {
            if (selectedItem == ItemMaster.ItemType.None) {
                selectObject.SetActive(false);
            }
            else {
                selectObject.SetActive(true);
                ItemButtonData itemButton = itemButtons.Find(ib => ib.itemMaster.type == selectedItem);
                if (itemButton.buttonObject) {
                    selectObject.transform.localPosition = itemButton.buttonObject.transform.localPosition;
                }
                else {
                    selectObject.SetActive(false);
                    selectedItem = ItemMaster.ItemType.None;
                }
            }
        }
    }
}
