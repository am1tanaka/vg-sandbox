﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
[CreateAssetMenu(menuName ="VG/Create EventData")]
public class EventData : ScriptableObject {
    [Tooltip("イベントデータのセット"), SerializeField]
    EventDataSet[] eventData;
    public EventDataSet[] EventDataSets {
        get {
            return eventData;
        }
    }

    /// <summary>
    /// 現在のデータを削除して、新しいデータセットを設定します。
    /// </summary>
    /// <param name="input"></param>
    public void SetEventDataSet(EventDataSet[] input) {
        eventData = new EventDataSet[input.Length];
        for (int i=0; i<input.Length;i++) {
            eventData[i] = new EventDataSet(input[i]);
        }
    }

    /// <summary>
    /// 引数で渡したイベントデータの条件を
    /// このEventDataが全て満たしていたらtrueを返します。
    /// </summary>
    /// <param name="check">チェックする条件を設定したイベントデータ</param>
    /// <returns>checkで渡した条件を全て満たしていたらtrue</returns>
    public bool Check(EventDataSet[] check) {
        for (int i=0; i<check.Length;i++) {
            bool flag = Check(check[i]);
            // 1つでもfalseなら不成立
            if (!flag) {
                return false;
            }
        }

        // 全部trueならtrue
        return true;
    }

    /// <summary>
    /// 指定の条件がこのEventDataで成立しているかをチェックします。
    /// valueは強制的に小文字でチェックするので、大文字小文字は区別しません。
    /// </summary>
    /// <param name="check">チェックするEventDataSet</param>
    /// <returns>指定の条件が成立している時true</returns>
    public bool Check(EventDataSet check) {
        for (int i=0; i<eventData.Length;i++) {
            if (check.name == eventData[i].name) {
                return (check.value.ToLower() == eventData[i].value.ToLower());
            }
        }

        // 定義がなければfalse
        return false;
    }

    /// <summary>
    /// 指定のデータを設定します。データの種類は既存のものとします。
    /// </summary>
    /// <param name="change">変更するデータセット</param>
    public void ChangeData(EventDataSet[] change) {

    }
}
