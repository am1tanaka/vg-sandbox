﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EventDataSet {
    /// <summary>
    /// データ名リスト。全てのステージで利用するデータ名をここに定義します。
    /// 数を把握するために最後にCOUNTを置きます。
    /// </summary>
    public enum DATA_NAME {
        ANIME,
        BED,
        CAMERA,
        CLEAR,
        CURTAIN,
        CUSHION,
        ITEM,
        LIGHT,
        PAPER,
        POSTER,
        REDBOX,
        USE,
        KEY,
        TRASH,
        COUNT
    }

    /// <summary>
    /// データ名
    /// </summary>
    public DATA_NAME name;
    /// <summary>
    /// データの値
    /// </summary>
    public string value;

    public EventDataSet(EventDataSet src) {
        Set(src);
    }

    /// <summary>
    /// データを設定する。
    /// </summary>
    /// <param name="src">コピー元データ</param>
    public void Set(EventDataSet src) {
        name = src.name;
        value = src.value;
    }

    /// <summary>
    /// 指定のEventDataSetと等しい場合trueを返します。
    /// </summary>
    /// <param name="dst">対象のEventDataSet</param>
    /// <returns>等しい場合、true。</returns>
    public bool EqualData(EventDataSet dst) {
        //Debug.Log("EqualData " + name + "/dst=" + dst);
        return (name == dst.name)
            && (value.ToLower() == dst.value.ToLower());
    }

    /// <summary>
    /// srcのデータが、dstのデータ内ですべて成立している時、
    /// trueを返します。
    /// </summary>
    /// <param name="src">チェックする元のデータセット</param>
    /// <param name="dst">srcがすべて成立しているかをチェックする対象のデータセット</param>
    /// <param name="changes">srcにchangesが含まれ</param>
    /// <returns>srcがdstですべて成立していたらtrueを返します。</returns>
    public static bool CheckDataIn(EventDataSet[] src, EventDataSet[] dst, EventDataSet[] changes=null) {
        // changesがnullの時は、changesのチェックを予めtrueにしておく
        bool isMatchChanges = (changes == null);

        // srcをループして、1つでも不成立ならその場でfalse
        for (int i = 0; i < src.Length; i++) {
            bool isMatchStore = false;
            if (changes != null) {
                for (int j = 0; j < changes.Length; j++) {
                    if (src[i].EqualData(changes[j])) {
                        isMatchChanges = true;
                        isMatchStore = true;
                        break;
                    }
                }
            }

            // この時点で一致していたら次のsrcデータをチェック
            if (isMatchStore) continue;

            for (int j = 0; j < dst.Length; j++) {
                if (src[i].EqualData(dst[j])) {
                    isMatchStore = true;
                    break;
                }
            }

            // 見つからなかった場合、不成立
            if (!isMatchStore) {
                return false;
            }
        }

        // 全部見つかって、changesのデータに1つでも一致していたらtrue
        return isMatchChanges;
    }

    /// <summary>
    /// changeの名前を探して、そのインデックスにデータを設定します。
    /// </summary>
    /// <param name="set">設定先の配列</param>
    /// <param name="changes">設定するデータ</param>
    public static void ChangeData(EventDataSet[] set, EventDataSet []changes) {
        for (int i=0; i<changes.Length;i++) {
            ChangeData(set, changes[i]);
        }
    }

    public static void ChangeData(EventDataSet[] set, EventDataSet change) {
        for (int j = 0; j < set.Length; j++) {
            if (set[j].name == change.name) {
                set[j].value = change.value;
                break;
            }
        }
    }

}

