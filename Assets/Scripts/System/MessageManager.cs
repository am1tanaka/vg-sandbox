﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

namespace VG {
    public class MessageManager : MonoBehaviour {
        public static MessageManager Instance {
            get;
            private set;
        }

        #region Serialize Fields

        [Tooltip("メッセージアセット"), SerializeField]
        TextAsset messageAsset;
        [Tooltip("クリック待ちアイコン"), SerializeField]
        Animator waitClickAnimator;
        [Tooltip("1文字の表示秒数"), SerializeField]
        float wordSecond = 0.02f;
        [Tooltip("文字効果音を鳴らす間隔"), SerializeField]
        float wordSESecond = 0.1f;

        [Header("デバッグ")]
        [SerializeField]
        bool isDebug = true;

        #endregion Serialize Fields

        #region Properties

        /// <summary>
        /// ウィンドウが開いている時、True
        /// </summary>
        public static bool isWindowOpen {
            get;
            private set;
        }
        Dictionary<string, List<string>> messages = new Dictionary<string, List<string>>();
        TextMeshProUGUI messageText;
        Animator messageAnimator;
        /// <summary>
        /// 表示するメッセージ
        /// </summary>
        string message;

        /// <summary>
        /// メッセージ完了時に呼び出す処理
        /// </summary>
        UnityAction messageDoneHandler = null;



        #endregion Properties

        #region System

        private void Awake() {
            Instance = this;
            messageText = GetComponentInChildren<TextMeshProUGUI>();
            messageAnimator = GetComponentInChildren<Animator>();
        }

        void Start() {
            string[] lines = messageAsset.text.Split('\n');
            foreach (string line in lines) {
                string[] cols = line.Split(',');
                if (cols.Length <= 1) {
                    continue;
                }
                List<string> add = new List<string>();
                for (int i = 1; i < cols.Length; i++) {
                    add.Add(cols[i]);
                }
                messages.Add(cols[0], add);
                add = null;
            }
        }

#if UNITY_EDITOR
        private void Update() {
            if (isDebug) {
                if (Input.GetKeyDown(KeyCode.M)) {
                    if (!isWindowOpen) {
                        ShowMessage(string.Format(GetMessage("get-item"), "赤い箱"));
                    }
                    else {
                        messageAnimator.SetBool("Open", false);
                    }

                    isWindowOpen = !isWindowOpen;
                }
            }
        }
#endif

        #endregion System

        /// <summary>
        /// 指定のメッセージを表示します。メッセージウィンドウが閉じた時に処理を
        /// したい場合、第２引数にUnityActionのコールバックを渡します。
        /// </summary>
        /// <param name="mes">表示メッセージ</param>
        /// <param name="done">完了時に呼び出す関数</param>
        public void ShowMessage(string mes, UnityAction done=null) {
            if (isWindowOpen) {
#if UNITY_EDITOR
                Debug.Log("メッセージ表示中なので、キャンセルします。");
#endif
                return;
            }

            messageDoneHandler = done;
            isWindowOpen = true;
            messageText.text = "";
            messageAnimator.SetBool("Open", true);
            message = mes;
            UIManager.Instance.SetInteractiveButtons(false);
        }

        public void MessageWindowOpenDone() {
            StartCoroutine(printMessage());
        }

        IEnumerator printMessage() {
            float time = 0;
            int wordCount = 0;
            float nextSETime = 0;
            while (wordCount < message.Length - 1) {
                time += Time.deltaTime;
                wordCount = (int)(time / wordSecond);
                if ((wordCount > message.Length - 1) || Input.GetButtonDown("Fire1")) {
                    wordCount = message.Length - 1;
                }

                messageText.text = message.Substring(0, wordCount + 1);
                if (nextSETime <= time) {
                    SoundPlayer.PlaySE(SoundPlayer.SE.ui_typing);
                    nextSETime += wordSESecond;
                }
                yield return null;
            }

            // クリック待ち
            waitClickAnimator.SetBool("Waiting", true);
            while (!Input.GetButtonDown("Fire1")) {
                yield return null;
            }

            // ウィンドウを閉じる
            SoundPlayer.PlaySE(SoundPlayer.SE.ui_page);
            waitClickAnimator.SetBool("Waiting", false);
            messageAnimator.SetBool("Open", false);
            isWindowOpen = false;
            UIManager.Instance.SetInteractiveButtons(true);
            messageDoneHandler?.Invoke();
        }

        /// <summary>
        /// メッセージを返します。
        /// </summary>
        /// <param name="key">キーを指定</param>
        /// <param name="idx">取り出すインデックス。省略すると最初のものを返します</param>
        /// <returns>文字列</returns>
        public string GetMessage(string key, int idx = 0) {
            if (!messages.ContainsKey(key)) {
                return "キー" + key + "が見つかりませんでした。";
            }

            if (messages[key].Count == 0) {
                return "キー" + key + "に値が含まれていませんでした。";
            }
            return messages[key][idx];
        }
    }
}
