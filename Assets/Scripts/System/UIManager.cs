﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.UI;

namespace VG {
    /// <summary>
    /// 各種ユーザーインターフェースを統括して管理するクラス
    /// </summary>
    public class UIManager : MonoBehaviour {

        #region Public Properties

        public static UIManager Instance { get; private set; }

        /// <summary>
        /// UIの状態として、部屋をクリックできる時、trueを返します。
        /// </summary>
        public static bool isClickDetectable {
            get {
                return !MessageManager.isWindowOpen;
            }
        }

        #endregion Public Properties

        #region Properties

        ReadOnlyCollection<Button> uiButtons;

        #endregion Properties

        #region System

        private void Awake() {
            Button[] btns = GetComponentsInChildren<Button>();
            uiButtons = new ReadOnlyCollection<Button>(btns);
            Instance = this;
        }

        #endregion System

        #region Public Methods

        /// <summary>
        /// 全てのボタンに押せるかどうかを設定します。
        /// </summary>
        /// <param name="flag"></param>
        public void SetInteractiveButtons(bool flag) {
            for (int i=0;i<uiButtons.Count;i++) {
                uiButtons[i].interactable = flag;
            }
        }

        #endregion Public Methods


    }
}
