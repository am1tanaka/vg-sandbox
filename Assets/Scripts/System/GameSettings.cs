﻿using UnityEngine;

[CreateAssetMenu(menuName ="VG/Create GameSettings")]
public class GameSettings : ScriptableObject {
    /// <summary>
    /// アプリのバージョン
    /// </summary>
    public string Version = "Ver 0.0.1";
}
