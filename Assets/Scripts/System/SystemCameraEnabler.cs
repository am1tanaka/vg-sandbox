﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Camera), typeof(AudioListener))]
public class SystemCameraEnabler : MonoBehaviour {
    private Camera systemCamera;
    private AudioListener audioListener;

	// Use this for initialization
	void Awake () {
        systemCamera = GetComponent<Camera>();
        audioListener = GetComponent<AudioListener>();
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }

    void OnSceneLoaded(Scene i_loadedScene, LoadSceneMode i_mode) {
        systemCamera.enabled = audioListener.enabled = false;
    }

    void OnSceneUnloaded(Scene i_unloaded) {
        systemCamera.enabled = audioListener.enabled = true;
    }


}
