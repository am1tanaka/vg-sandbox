﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace VG {
    public class ItemTest : MonoBehaviour {

        ItemListManager itemListManager;

        // Use this for initialization
        void Start() {
            itemListManager = GetComponent<ItemListManager>();
        }

        // Update is called once per frame
        void Update() {
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                itemListManager.GetItem(ItemMaster.ItemType.Kami);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2)) {
                itemListManager.GetItem(ItemMaster.ItemType.Key);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3)) {
                itemListManager.GetItem(ItemMaster.ItemType.RedBox);
            }
            else if (Input.GetKeyDown(KeyCode.Q)) {
                itemListManager.ReleaseItem(ItemMaster.ItemType.Kami);
            }
            else if (Input.GetKeyDown(KeyCode.W)) {
                itemListManager.ReleaseItem(ItemMaster.ItemType.Key);
            }
            else if (Input.GetKeyDown(KeyCode.E)) {
                itemListManager.ReleaseItem(ItemMaster.ItemType.RedBox);
            }
        }
    }
}
