﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VersionText : MonoBehaviour {
    private void Awake() {
        GetComponent<Text>().text = GameSystem.Instance.Settings.Version;
    }
}
