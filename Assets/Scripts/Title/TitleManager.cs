﻿using UnityEngine;

public class TitleManager : MonoBehaviour {

    // Use this for initialization
    void Start () {
        SoundPlayer.PlayBGM(SoundPlayer.BGM.NONE);
	}

	// Update is called once per frame
	void Update () {
        // 操作可能な状態かをチェック
        if (!GameSystem.IsControllable) {
            return;
        }

        // クリックされたらゲーム画面へ
        if (Input.GetMouseButtonDown(0)) {
            SoundPlayer.PlaySE(SoundPlayer.SE.ui_page);
            GameSystem.ChangeScene("Game");
        }
	}
}
